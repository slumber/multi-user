### Problem to solve

<!-- Include the following detail as necessary:
* What feature(s) affected?
* What docs or doc section affected? Include links or paths.
* Is there a problem with a specific document, or a feature/process that's not addressed sufficiently in docs?
* Any other ideas or requests?
-->

### Further details

<!--
* Any concepts, procedures, reference info we could add to make it easier to successfully use the multi-user addom?
* Include use cases, benefits, and/or goals for this work.
-->

### Proposal

<!-- Further specifics for how can we solve the problem. -->

### Who can address the issue

<!-- What if any special expertise is required to resolve this issue? -->

### Other links/references

<!-- E.g. related GitLab issues/MRs -->

/label ~type::documentation
/cc @project-manager
